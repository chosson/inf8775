////////////////////////////////////////////////////////////////////////////////
/// \file main.cpp
/// \author Charles Hosson
/// \author Salha Yahia
////////////////////////////////////////////////////////////////////////////////


#pragma region "Includes" //{

#include <ciso646>
#include <cstddef>
#include <cstdint>
#include <new>

#include <algorithm>
#include <chrono>
#include <cmath>
#include <fstream>
#include <functional>
#include <iomanip>
#include <iostream>
#include <map>
#include <numeric>
#include <random>
#include <stdexcept>
#include <string>
#include <thread>
#include <type_traits>
#include <unordered_map>
#include <unordered_set>
#include <utility>
#include <vector>

#if defined(_WIN32)
	#include <windows.h>
#elif defined(__unix__)
	#include <sys/time.h>
	#include <sys/resource.h>
#endif


using namespace std::literals;

#pragma endregion //}



#pragma region "Declarations" //{

#define RUN_AND_MEASURE(timesVar, ...) \
{ \
	Durations t1 = getTimes(); \
	__VA_ARGS__; \
	Durations t2 = getTimes(); \
	timesVar = {t2.userTime - t1.userTime, t2.sysTime - t1.sysTime, t2.realTime - t1.realTime}; \
} \


static constexpr int NUM_PROBABILISTIC_ITERATIONS = 10;
static constexpr double HEURISTICS_TIMEOUT_SECONDS = 5 * 60.0;


struct ProgramOptions
{
	std::string algorithmName;
	std::string dataFilename;
	bool        printTime = false;
	bool        printStores = false;
	bool        printTotalRevenue = false;
};

struct Durations
{
	double userTime;
	double sysTime;
	double realTime;
};

struct Store
{
	int    id;
	int    revenue;
	int    requiredSupply;
	double efficiency;
};

namespace std
{
	template<>
	struct hash<Store>
	{
		using argument_type = Store;
		using result_type = size_t;
		result_type operator () ( const argument_type& v ) const noexcept
		{
			return std::hash<int>{}(v.id);
		}
	};
}

bool operator == ( const Store& lhs, const Store& rhs ) { return lhs.id == rhs.id; }

using StoreVec = std::vector<Store>;
using StoreSet = std::unordered_set<Store>;
using StoreMap = std::unordered_map<int, Store>;

struct City
{
	StoreVec stores;
	int      supplierCapacity;
};

using DynamicSolutionArray = std::vector<std::vector<int>>;


Durations getTimes ( );

ProgramOptions parseArgs ( int&, char** );

City readFile ( const std::string& );

int computeTotalRevenue ( const StoreVec& );

int computeTotalRequiredSupply ( const StoreVec& );

double computeTotalEfficiency ( const StoreVec& );

void computeEfficiencies ( StoreVec& );

StoreMap vecToMap ( const StoreVec& );

StoreVec mapToVec ( const StoreMap& );

Store extractRandomElementFromEfficiency ( StoreVec&, double& );

void applyProbabilisticGreedy ( const City&, StoreVec& );

void applyDynamicProgramming ( const City&, StoreVec& );

DynamicSolutionArray buildDynamicSolutionArray ( const City& );

void applyLocalImprovementHeuristics ( const City&, StoreVec& );

#pragma endregion //}


int main ( int argc, char* argv[] )
{
	ProgramOptions opts = parseArgs(argc, argv);

	City data = readFile(opts.dataFilename);
	if ( data.stores.empty() ) {
		std::cout << "City in file " << std::quoted(opts.dataFilename) << " has no stores." << std::endl;
		return 0;
	}

	StoreVec results;
	results.reserve(data.stores.size());
	Durations times = {};

	if ( opts.algorithmName == "glouton" ) {
		RUN_AND_MEASURE(times,
			computeEfficiencies(data.stores);
			applyProbabilisticGreedy(data, results);
		)
	}
	else if ( opts.algorithmName == "progdyn" ) {
		RUN_AND_MEASURE(times,
			applyDynamicProgramming(data, results);
		)
	}
	else if ( opts.algorithmName == "local" ) {
		RUN_AND_MEASURE(times,
			computeEfficiencies(data.stores);
			applyLocalImprovementHeuristics(data, results);
		)
	}
	else {
		throw std::logic_error("Unrecognized algorithm '" + opts.algorithmName + "'");
	}

	if ( opts.printTime ) {
		std::cout << (times.realTime * 1000.0) << " ";
		#ifdef PRINT_USER_TIME
			std::cout << (times.userTime * 1000.0) << " ";
		#endif
		#ifdef PRINT_SYS_TIME
			std::cout << (times.sysTime * 1000.0) << " ";
		#endif
		std::cout << std::endl;
	}
	if ( opts.printTotalRevenue ) {
		int totalRevenue = computeTotalRevenue(results);
		std::cout << totalRevenue << std::endl;
	}
	if ( opts.printStores ) {
		for ( auto&& s : results )
			std::cout << s.id << " ";
		std::cout << std::endl;
	}
}


#pragma region "Functions" //{

Durations
getTimes ( )
{
	Durations times = {};

	#if defined(_WIN32)
		// User time is given in number of 100 nanoseconds intervals.
		static constexpr double INTERVALS_PER_SEC = 10'000'000.0;

		uint64_t userTimeRaw;
		uint64_t kernelTimeRaw;
		FILETIME dummy;
		GetProcessTimes(GetCurrentProcess(), &dummy, &dummy, (FILETIME*)&kernelTimeRaw, (FILETIME*)&userTimeRaw);
		times.userTime = userTimeRaw / INTERVALS_PER_SEC;
		times.sysTime = kernelTimeRaw / INTERVALS_PER_SEC;
	#elif defined(__unix__)
		rusage usage;
		getrusage(RUSAGE_SELF, &usage);
		timeval userTimeRaw = usage.ru_utime;
		uint64_t microseconds = userTimeRaw.tv_sec * 1'000'000 + userTimeRaw.tv_usec;
		times.userTime = microseconds / 1'000'000.0;
		timeval sysTimeRaw = usage.ru_stime;
		microseconds = sysTimeRaw.tv_sec * 1'000'000 + sysTimeRaw.tv_usec;
		times.sysTime = microseconds / 1'000'000.0;
	#endif

	auto now = std::chrono::high_resolution_clock::now().time_since_epoch();
	times.realTime =  std::chrono::duration_cast<std::chrono::nanoseconds>(now).count() / 1.0e9;

	return times;
}


ProgramOptions
parseArgs ( int& argc, char** argv )
{
	ProgramOptions opts = {};

	if ( argc < 3 )
		throw std::logic_error("Must have algorithm and data set filename");
	opts.algorithmName = argv[1];
	opts.dataFilename = argv[2];

	for ( int i = 3; i < argc; i++ ) {
		if ( argv[i] == "-t"s )
			opts.printTime = true;
		else if ( argv[i] == "-p"s )
			opts.printStores = true;
		else if ( argv[i] == "--print-total-revenue"s )
			opts.printTotalRevenue = true;
	}

	return opts;
}


City
readFile ( const std::string& filename )
{
	std::fstream file(filename, std::ios::in);
	file.exceptions(std::ios::failbit);

	City city = {};
	size_t numStores;
	file >> numStores;
	city.stores.resize(numStores);
	for ( auto&& s : city.stores )
		file >> s.id >> s.revenue >> s.requiredSupply;
	file >> city.supplierCapacity;

	return city;
}


int
computeTotalRevenue ( const StoreVec& stores )
{
	int result = 0;
	for ( auto&& s : stores )
		result += s.revenue;
	return result;
}


int
computeTotalRequiredSupply ( const StoreVec& stores )
{
	int result = 0;
	for ( auto&& s : stores )
		result += s.requiredSupply;
	return result;
}


double
computeTotalEfficiency ( const StoreVec& stores)
{
	double result = 0.0;
	for ( auto&& s : stores )
		result += s.efficiency;
	return result;
}


void
computeEfficiencies ( StoreVec& stores )
{
	for ( auto&& s : stores )
		s.efficiency = (double)s.revenue / s.requiredSupply;
}


StoreMap
vecToMap ( const StoreVec& stores )
{
	StoreMap result;
	for ( auto&& s : stores )
		result[s.id] = s;
	return result;
}


StoreVec
mapToVec ( const StoreMap& stores )
{
	StoreVec result;
	for ( auto&& [id, s] : stores )
		result.push_back(s);
	return result;
}


Store
extractRandomElementFromEfficiency ( StoreVec& stores, double& totalEfficiency )
{
	static thread_local std::random_device gen;
	static thread_local std::uniform_real_distribution dist(0.0, 1.0);
	static thread_local auto getRand = std::bind(std::ref(dist), std::ref(gen));

	size_t index = 0;
	while ( true ) {
		index = (size_t)(getRand() * stores.size());
		if ( getRand() < stores[index].efficiency / totalEfficiency )
			break;
	}

	Store selected = std::move(stores[index]);
	stores.erase(stores.begin() + index);
	totalEfficiency -= selected.efficiency;

	return std::move(selected);
}


void
applyProbabilisticGreedy ( const City& data, StoreVec& results )
{
	int maxRevenue = 0;
	StoreVec selectedStores;
	StoreVec potentialStores;
	selectedStores.reserve(results.capacity());

	for ( int i = 0; i < NUM_PROBABILISTIC_ITERATIONS; i++ ) {
		selectedStores.clear();
		potentialStores = data.stores;

		int remainingSupply = data.supplierCapacity;
		double totalEfficiency = computeTotalEfficiency(potentialStores);
		int totalRevenue = 0;
		while ( not potentialStores.empty() and remainingSupply >= 1 ) {
			Store selected = extractRandomElementFromEfficiency(potentialStores, totalEfficiency);
			if ( selected.requiredSupply <= remainingSupply ) {
				remainingSupply -= selected.requiredSupply;
				totalRevenue += selected.revenue;
				selectedStores.push_back(std::move(selected));
			}
		}

		if ( totalRevenue > maxRevenue ) {
			maxRevenue = totalRevenue;
			std::swap(selectedStores, results);
		}
	}
}


void
applyDynamicProgramming ( const City& city, StoreVec& results )
{
	DynamicSolutionArray solutionArray = buildDynamicSolutionArray(city);

	const auto& stores = city.stores;
	int remainingSupply = city.supplierCapacity;
	int totalRequiredSupply = 0;
	for ( auto i = (ptrdiff_t)solutionArray.size() - 1; i >= 0; i-- ) {
		if ( i == 0 ) {
			if ( remainingSupply - stores[i].requiredSupply > 0 )
				results.push_back(stores[i]);
		}
		else if ( solutionArray[i][remainingSupply] != solutionArray[i - 1][remainingSupply] ) {
			if ( remainingSupply - stores[i].requiredSupply >= 0 ) {
				totalRequiredSupply += stores[i].requiredSupply;
				results.push_back(stores[i]);
				remainingSupply -= stores[i].requiredSupply;
			}
		}
	}
}


DynamicSolutionArray
buildDynamicSolutionArray ( const City& city )
{
	const auto& stores = city.stores;
	DynamicSolutionArray solutionArray(stores.size());
	for ( auto&& x : solutionArray )
		x.resize(city.supplierCapacity + 1);

	for ( size_t i = 0; i < stores.size(); i++ ) {
		for ( int j = 0; j < city.supplierCapacity + 1; j++ ) {
			if ( i == 0 ) {
				if ( stores[i].requiredSupply <= j )
					solutionArray[i][j] = stores[i].revenue;
			}
			else if ( j < stores[i].requiredSupply ) {
				solutionArray[i][j] = solutionArray[i - 1][j];
			}
			else {
				solutionArray[i][j] = std::max(stores[i].revenue + solutionArray[i - 1][j - stores[i].requiredSupply], solutionArray[i - 1][j]);
			}
		}
	}

	return solutionArray;
}


void
applyLocalImprovementHeuristics ( const City& city, StoreVec& results )
{
	applyProbabilisticGreedy(city, results);
	const auto& stores = city.stores;
	int remainingCapacity = city.supplierCapacity - computeTotalRequiredSupply(results);
	int totalRevenue = computeTotalRevenue(results);

	double startTime = getTimes().userTime;
	bool modified = true;
	while ( modified ) {
		modified = false;
		for ( size_t i = 0; i < results.size(); i++ ) {
			for ( size_t j = 0; j < stores.size(); j++ ) {
				auto isSameStoreId = [&]
				( const Store& store )
				{
					return store.id == (int)j;
				};
				bool isStoreInResults = std::find_if(results.begin(), results.end(), isSameStoreId) != results.end();
				if ( not isStoreInResults ) {
					int newCapacity = remainingCapacity + results[i].requiredSupply - stores[j].requiredSupply;
					int newRevenue = totalRevenue - results[i].revenue + stores[j].revenue;

					if ( newCapacity >= 0 and newRevenue > totalRevenue ) {
						// HACK : It's not specifically stated that store IDs are ordered from 1 to n_stores, but that's what's in the files. Should be a linear search for the store with the given ID, but I'll just do ID - 1. k thx bye.
						results.erase(results.begin() + i);
						results.insert(results.begin() + i, stores[j]);
						remainingCapacity = newCapacity;
						totalRevenue = newRevenue;
						modified = true;
					}
				}
				if ( getTimes().userTime - startTime > HEURISTICS_TIMEOUT_SECONDS )
					return;
			}
		}
	}
}

#pragma endregion //}

