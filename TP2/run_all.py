import collections
import os.path
import sys
import subprocess


def find_all_files_in_dir(dir_path):
	all_files = []
	for root, subdirs, files in os.walk(dir_path):
		for f in files:
			filename = root + "/" + f
			filename = filename.replace("\\", "/")
			filename = filename.replace(dir_path.replace("\\", "/"), "")
			if filename[0] == "/":
				filename = filename[1:len(filename)]
			all_files.append(filename)
	
	return all_files


testset_files_dir = "exemplaires/"
print("Parsing directory '%s'..." % (testset_files_dir), flush=True)
all_files = find_all_files_in_dir(testset_files_dir)
if len(all_files) == 0:
	print("Directory '%s' is empty." % (testset_files_dir), flush=True)
print("Directory '%s' has %d files." % (testset_files_dir, len(all_files)), flush=True)

# Add filenames in this dict to skip those files.
excluded_files = {}
excluded_files["glouton"] = {}
excluded_files["progdyn"] = {}
excluded_files["local"] =   {}

exec_timeout = 10 * 60.0

if not os.access("results", os.F_OK):
	os.mkdir("results");

print("", flush=True)
results = {}
for algo in ["glouton", "progdyn", "local"]:
	results[algo] = {}
	error_filename = "results/errors-%s.txt" % (algo)
	open(error_filename, "w+").close()
	for testset_filename in all_files:
		if testset_filename in excluded_files[algo]:
			continue
		path = os.path.realpath("bin/INF8775_TP2")
		split_filename = testset_filename.split("-")
		example_size = int(split_filename[1])
		series_no = int(split_filename[2])
		example_no = int(split_filename[3].split(".")[0])
		proc_cmd = [path] + [algo] + ["exemplaires/" + testset_filename] + ["-t"] + ["--print-total-revenue"]
		completed_with_errors = False
		# Log the files that take too long or that cause an error (most likely unhandled exception, hence returncode !=0).
		try:
			proc_result = subprocess.run(proc_cmd, stdout=subprocess.PIPE, timeout=exec_timeout)
		except subprocess.TimeoutExpired:
			completed_with_errors = True
		else:
			completed_with_errors = proc_result.returncode != 0
		
		try:
			stdout_str = proc_result.stdout.decode("utf-8")
			testset_result = [float(s) for s in stdout_str.split()]
		except:
			completed_with_errors = True
		
		print("%s: %d-%d-%d" % (algo, example_size, series_no, example_no), end=", ", flush=True)
		
		if completed_with_errors:
			with open(error_filename, "a") as error_file:
				error_file.write(testset_filename + "\n")
			continue
		
		# Add the result in the dict.
		if example_size not in results[algo]:
			results[algo][example_size] = {}
		if series_no not in results[algo][example_size]:
			results[algo][example_size][series_no] = {}
		results[algo][example_size][series_no][example_no] = testset_result
	print("")

# Write the results to file with the date in the proper numerical order.
for algo in results:
	with open("results/" + algo + ".csv", "w+") as results_file:
		results_file.write("Test set size\tTest set series\tTest set no\tReal time\tUser time\tSystem time\tTotal revenue" + "\n")
		for size in sorted(results[algo]):
			for ser_no in sorted(results[algo][size]):
				for ex_no in sorted(results[algo][size][ser_no]):
					result = results[algo][size][ser_no][ex_no]
					results_file.write("%d\t%d\t%d\t%f\t%f\t%f\t%f\n" % (size, ser_no, ex_no, result[0], result[1], result[2], result[3]))

