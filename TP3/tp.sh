#!/bin/bash

OPTIONS=""
while [[ $# -gt 0 ]]
do
key="$1"

case $key in
    -e)
    EX_PATH="$2"
    shift
    ;;
    -t|-p)
    OPTIONS="${OPTIONS} ${1} "
    ;;
    -n|-c)
    OPTIONS="${OPTIONS} ${1} ${2} "
    shift
    ;;
    *)
        echo "Argument inconnu: ${1}"
        exit
    ;;
esac
shift
done

./bin/INF8775_TP3 $ALGO $EX_PATH $OPTIONS
