$example = $args[0]
$num_iter = $args[1]
$PSDefaultParameterValues['Out-File:Encoding'] = 'ascii'
$input_file = "exemplaires/LEGO_$example"
$output_file = "results/sol_$example.txt"
bash ./tp.sh -e $input_file -n $num_iter > $output_file
py sol_check.py $input_file $output_file