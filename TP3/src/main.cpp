////////////////////////////////////////////////////////////////////////////////
/// \file main.cpp
/// \author Charles Hosson
/// \author Salha Yahia
////////////////////////////////////////////////////////////////////////////////


#pragma region "Includes" //{

#include <ciso646>
#include <cstddef>
#include <cstdint>
#include <new>

#include <algorithm>
#include <atomic>
#include <chrono>
#include <cmath>
#include <cstdlib>
#include <ctime>
//#include <execution>
#include <fstream>
#include <functional>
#include <iomanip>
#include <iostream>
#include <map>
#include <numeric>
#include <random>
#include <stdexcept>
#include <string>
#include <thread>
#include <type_traits>
#include <unordered_map>
#include <unordered_set>
#include <utility>
#include <vector>

#if defined(_WIN32)
	#include <windows.h>
#elif defined(__unix__)
	#include <sys/time.h>
	#include <sys/resource.h>
#endif


using namespace std::literals;

#pragma endregion //}



#pragma region "Declarations" //{

#define RUN_AND_MEASURE(timesVar, ...) \
{ \
	Durations t1 = getTimes(); \
	__VA_ARGS__; \
	Durations t2 = getTimes(); \
	timesVar = t2 - t1; \
} \


struct ProgramOptions
{
	std::string dataFilename;
	std::string configFilename = "params.txt";
	bool        printTotalCost = false;
	bool        printTimes = false;
	size_t      numEvolutions = 0;
};

struct Durations
{
	double userTime;
	double sysTime;
	double realTime;
};

using Quantity = int;
using Price = int;
using ModelsArray = std::vector<size_t>;
using QuantityArray = std::vector<Quantity>;
using PricesArray = std::vector<Price>;

struct LegoModel
{
	QuantityArray pieces;
	PricesArray   prices;
	Price         totalPrice;
};

struct ProblemData
{
	size_t                 numPieces;
	size_t                 numModels;
	QuantityArray          ownedPieces;
	PricesArray            piecePrices;
	std::vector<LegoModel> models;
};

struct AlgoParameters
{
	Price  greedyRandThreshold;
	size_t numMutations;
	size_t numIndividuals;
	size_t numSurvivors;
	size_t numRegressIterations;
	size_t numUnchangedGenerationsMax;
};

class Individual;

class LegoGeneticAlgo
{
	// Friends
	friend class Individual;

public:
	// Nested Types
	using Population = std::vector<Individual>;

	// Modifying Methods
	void init ( const std::string&, const std::string& );
	void evolve ( );

	// Non-Modifying Methods
	const ProblemData& getProblemData ( ) const { return m_problem; }
	const Individual&  getBestIndividual ( ) const;

private:
	// Modifying Methods
	void readDataFile ( const std::string& );
	void readConfigFile ( const std::string& );
	void initPopulation ( );
	void selectSurvivors ( );
	void adapt ( size_t );
	void runParallelMutations ( );
	void updateCounters ( );

	// Non-Modifying Methods
	ModelsArray findBestModelsByPiece ( ) const;

	// Data Members
	AlgoParameters m_params;
	ProblemData    m_problem;
	ModelsArray    m_bestModelsByPiece;
	Population     m_individuals;
	Population     m_survivors;
	size_t         m_generationNo = 0;
	Price          m_lastGenerationScore;
	size_t         m_numUnchangedGenerations = 0;
};

class Individual
{
public:
	// Constructors and Destructor
	Individual ( ) = default;
	Individual ( const Individual& ) = default;
	Individual ( LegoGeneticAlgo* parent ) : m_parent(parent) { }

	// Modifying Methods
	void initWithRandom ( Price = 0 );
	void initWithGreedy ( );
	void applyMutation ( );
	void applyRegression ( size_t );

	// Non-Modifying Methods
	LegoGeneticAlgo*     getParent ( ) const { return m_parent; }
	const QuantityArray& getModels ( ) const { return m_models; }
	Price                getTotalCost ( ) const { return m_totalCost; }

	void print ( std::ostream& ) const;
	bool isBetterThan ( const Individual& ) const;

private:
	// Modifying Methods
	void initCostsAndGenes ( );
	void addModel ( size_t );
	void removeModel ( size_t );
	void switchModels ( size_t, size_t );
	void doGreedyFill ( );
	void doRandomFill ( );

	// Non-Modifying Methods
	bool areAllCostsOverValue ( Price ) const;

	// Data Members
	LegoGeneticAlgo* m_parent = nullptr;
	QuantityArray    m_models;
	PricesArray      m_costs;
	Price            m_totalCost;
};


Durations getTimes ( );

Durations operator - ( const Durations&, const Durations& );

ProgramOptions parseArgs ( int&, char** );

unsigned getRand ( );

bool operator < ( const Individual&, const Individual& );

#pragma endregion //}


int main ( int argc, char* argv[] )
{
	srand((unsigned)time(nullptr));
	ProgramOptions opts = parseArgs(argc, argv);

	LegoGeneticAlgo algo;

	algo.init(opts.dataFilename, opts.configFilename);
	if ( algo.getProblemData().numPieces == 0 or algo.getProblemData().numModels == 0 ) {
		std::cout << "Data in file " << std::quoted(opts.dataFilename) << " has no pieces/models." << std::endl;
		return 0;
	}

	#define PRINT_STUFF(bestIndividual, times) \
		bestIndividual.print(std::cout); \
		if ( opts.printTotalCost ) \
			std::cout << bestIndividual.getTotalCost() << std::endl; \
		if ( opts.printTimes ) \
			std::cout << times.realTime << " " << times.userTime << " " << times.sysTime << std::endl; \

	bool hasFiniteLoop = opts.numEvolutions != 0;
	if ( hasFiniteLoop ) {
		Durations times;
		RUN_AND_MEASURE(times,
			for ( size_t i = 0; i < opts.numEvolutions; i++ )
				algo.evolve();
		)
		PRINT_STUFF(algo.getBestIndividual(), times)
	}
	else {
		while ( true ) {
			Durations times;
			RUN_AND_MEASURE(times,
				algo.evolve();
			)
			PRINT_STUFF(algo.getBestIndividual(), times)
		}
	}
}


#pragma region "Functions" //{

Durations
getTimes ( )
{
	Durations times = {};

	#if defined(_WIN32)
		// User time is given in number of 100 nanoseconds intervals.
		static constexpr double INTERVALS_PER_SEC = 10'000'000.0;

		uint64_t userTimeRaw;
		uint64_t kernelTimeRaw;
		FILETIME dummy;
		GetProcessTimes(GetCurrentProcess(), &dummy, &dummy, (FILETIME*)&kernelTimeRaw, (FILETIME*)&userTimeRaw);
		times.userTime = userTimeRaw / INTERVALS_PER_SEC;
		times.sysTime = kernelTimeRaw / INTERVALS_PER_SEC;
	#elif defined(__unix__)
		rusage usage;
		getrusage(RUSAGE_SELF, &usage);
		timeval userTimeRaw = usage.ru_utime;
		uint64_t microseconds = userTimeRaw.tv_sec * 1'000'000 + userTimeRaw.tv_usec;
		times.userTime = microseconds / 1'000'000.0;
		timeval sysTimeRaw = usage.ru_stime;
		microseconds = sysTimeRaw.tv_sec * 1'000'000 + sysTimeRaw.tv_usec;
		times.sysTime = microseconds / 1'000'000.0;
	#endif

	auto now = std::chrono::high_resolution_clock::now().time_since_epoch();
	times.realTime =  std::chrono::duration_cast<std::chrono::nanoseconds>(now).count() / 1.0e9;

	return times;
}

Durations
operator - ( const Durations& lhs, const Durations& rhs )
{
	return {lhs.userTime - rhs.userTime, lhs.sysTime - rhs.sysTime, lhs.realTime - rhs.realTime};
}

ProgramOptions
parseArgs ( int& argc, char** argv )
{
	ProgramOptions opts = {};

	if ( argc < 2 )
		throw std::logic_error("Must have data set dataFilename");
	opts.dataFilename = argv[1];

	for ( int i = 2; i < argc; i++ ) {
		if ( argv[i] == "-n"s )
			opts.numEvolutions = atoi(argv[++i]);
		if ( argv[i] == "-c"s )
			opts.configFilename = argv[++i];
		if ( argv[i] == "-p"s )
			opts.printTotalCost = true;
		if ( argv[i] == "-t"s )
			opts.printTimes = true;
	}

	return opts;
}

unsigned
getRand ( )
{
	static std::random_device gen;
	return gen();
}

bool
operator < ( const Individual& lhs, const Individual& rhs )
{
	return rhs.isBetterThan(lhs);
}

#pragma endregion //}


#pragma region "LegoGeneticAlgo" //{

#pragma region "Modifying Methods" //{

void
LegoGeneticAlgo::init( const std::string& dataFilename, const std::string& configFilename )
{
	*this = {};

	this->readDataFile(dataFilename);

	m_params = {100, 5, 200000, 100000, 4, 100}; // Default values if file not readable.
	this->readConfigFile(configFilename);

	m_bestModelsByPiece = this->findBestModelsByPiece();
	this->initPopulation();
}

void
LegoGeneticAlgo::evolve ( )
{
	if ( m_numUnchangedGenerations < m_params.numUnchangedGenerationsMax ) {
		this->runParallelMutations();

		this->adapt(m_params.numRegressIterations);

		this->updateCounters();
	}
	else {
		// TODO: If too many generations in a row have given the same result, try something else.
	}

}


// private:

void
LegoGeneticAlgo::readDataFile ( const std::string& filename )
{
	std::fstream file(filename, std::ios::in);
	file.exceptions(std::ios::failbit);

	file >> m_problem.numPieces;
	m_problem.ownedPieces.resize(m_problem.numPieces);
	for ( auto&& p : m_problem.ownedPieces )
		file >> p;
	m_problem.piecePrices.resize(m_problem.numPieces);
	for ( auto&& p : m_problem.piecePrices )
		file >> p;
	file >> m_problem.numModels;
	m_problem.models.resize(m_problem.numModels);
	for ( size_t i = 0; i < m_problem.numModels; i++ ) {
		m_problem.models[i].pieces.resize(m_problem.numPieces);
		m_problem.models[i].prices.resize(m_problem.numPieces);
		for ( size_t j = 0; j < m_problem.numPieces; j++ ) {
			Quantity p;
			file >> p;
			m_problem.models[i].pieces[j] = p;
			m_problem.models[i].prices[j] = p * m_problem.piecePrices[j];
			m_problem.models[i].totalPrice += m_problem.models[i].prices[j];
		}
	}
}

void
LegoGeneticAlgo::readConfigFile ( const std::string& filename )
{
	std::fstream file(filename, std::ios::in);
	if ( file.fail() )
		return;

	file >> m_params.greedyRandThreshold
	     >> m_params.numMutations
	     >> m_params.numIndividuals
	     >> m_params.numSurvivors
	     >> m_params.numRegressIterations
	     >> m_params.numUnchangedGenerationsMax;
}

void
LegoGeneticAlgo::initPopulation ( )
{
	m_individuals.resize(m_params.numIndividuals, {this});
	for ( auto&& indiv : m_individuals )
		indiv.initWithGreedy();
}

void
LegoGeneticAlgo::selectSurvivors ( )
{
	std::sort(m_individuals.begin(), m_individuals.end());
	m_survivors.resize(m_params.numSurvivors);
	std::copy(m_individuals.end() - m_params.numSurvivors, m_individuals.end(), m_survivors.begin());
}

void
LegoGeneticAlgo::adapt ( size_t numIterations )
{
	this->selectSurvivors();

	for ( size_t i = 0; i < m_individuals.size() - m_params.numSurvivors; i++ ) {
		size_t parentIndex = getRand() % m_params.numSurvivors;
		auto& child = m_individuals[i];
		child = m_survivors[parentIndex];
		child.applyRegression(numIterations);
	}
}

void
LegoGeneticAlgo::runParallelMutations ( )
{
	//#ifdef PARALLEL_MUTATIONS
	#if true
		//auto fn = [] ( Individual& indiv ) { indiv.applyMutation(); return indiv; };
		//std::transform(std::execution::par_unseq, m_individuals.begin(), m_individuals.end(), m_individuals.begin(), fn);

		// Stupid GCC doesn't support <execution>, had to parallelize by hand :/
		auto& individuals = m_individuals;
		volatile std::atomic<size_t> nextIndex = 0;
		auto fn = [&] ( )
		{
			while ( true ) {
				size_t cur = nextIndex++;
				if ( cur >= individuals.size() )
					return;
				individuals[cur].applyMutation();
			}
		};

		std::vector<std::thread> threads;
		for ( size_t i = 0; i < std::thread::hardware_concurrency(); i++ )
			threads.push_back(std::thread(fn));
		for ( auto&& t : threads )
			t.join();
		threads.clear();
	#else
		auto fn = [] ( Individual& indiv ) { indiv.applyMutation(); return indiv; };
		std::transform(m_individuals.begin(), m_individuals.end(), m_individuals.begin(), fn);
	#endif
}

void
LegoGeneticAlgo::updateCounters ( )
{
	m_generationNo++;
	Price genScore = this->getBestIndividual().getTotalCost();
	if ( genScore == m_lastGenerationScore )
		m_numUnchangedGenerations++;
	else
		m_numUnchangedGenerations = 0;
	m_lastGenerationScore = genScore;
}

#pragma endregion //}

#pragma region "Non-Modifying Methods" //{

const Individual&
LegoGeneticAlgo::getBestIndividual ( )
const
{
	return m_individuals.back();
}

// private:

ModelsArray
LegoGeneticAlgo::findBestModelsByPiece ( )
const
{
	ModelsArray results(m_problem.numPieces);
	for ( size_t i = 0; i < m_problem.numPieces; i++ ) {
		size_t bestModel = 0;
		double bestScore = 0.0;
		for ( size_t j = 0; j < m_problem.numModels; j++ ) {
			Quantity pieceQty = m_problem.models[j].pieces[i];
			double score = (double)pieceQty / m_problem.models[j].totalPrice;
			if ( score > bestScore ) {
				bestScore = score;
				bestModel = j;
			}
		}
		results[i] = bestModel;
	}

	return results;
}

#pragma endregion //}

#pragma endregion //}


#pragma region "Individual" //{

#pragma region "Modifying Methods" //{

void
Individual::initWithRandom ( Price threshold )
{
	auto& problem = m_parent->getProblemData();
	this->initCostsAndGenes();

	while ( true ) {
		size_t model = getRand() % problem.numModels;
		this->addModel(model);
		if ( this->areAllCostsOverValue(threshold) )
			break;
	}
}

void
Individual::initWithGreedy ( )
{
	this->initWithRandom(-m_parent->m_params.greedyRandThreshold);

	this->doGreedyFill();
}

void
Individual::applyMutation ( )
{
	// TODO: Optimize this to avoid realloc every mutation (get an external buffer or something).
	Individual mutant = *this;
	for ( size_t i = 0; i < m_parent->m_params.numMutations; i++ ) {
		size_t given = getRand() % m_models.size();
		size_t taken = getRand() % m_models.size();
		for ( size_t j = 0; j < m_models.size(); j++ ) {
			++taken %= m_models.size();
			if ( mutant.m_models[taken] > 0 and mutant.m_costs[taken] > 0 ) {
				mutant.switchModels(taken, given);
				break;
			}
		}
	}

	// No point in going on if mutant is already worst.
	if ( mutant.m_totalCost > m_totalCost )
		return;

	// Once in a while, fill randomly to have some more diversity.
	if ( getRand() % 5 == 0 )
		mutant.doRandomFill();
	else
		mutant.doGreedyFill();

	if ( mutant.m_totalCost <= m_totalCost )
		std::swap(*this, mutant);
}

void
Individual::applyRegression ( size_t numIterations )
{
	// TODO: Optimize this to avoid realloc every mutation (get an external buffer or something).
	Individual child = *this;
	for ( size_t i = 0; i < numIterations; i++ ) {
		size_t taken = getRand() % m_models.size();
		for ( size_t j = 0; j < m_models.size(); j++ ) {
			++taken %= m_models.size();
			if ( child.m_models[taken] > 0 and child.m_costs[taken] > 0 ) {
				child.removeModel(taken);
				break;
			}
		}
	}

	// Once in a while, do a greedy fill to get some convergence.
	if ( getRand() % 5 == 0 )
		child.doGreedyFill();
	else
		child.doRandomFill();

	if ( child.m_totalCost <= m_totalCost )
		std::swap(*this, child);
}


// private:

void
Individual::initCostsAndGenes ( )
{
	auto& problem = m_parent->getProblemData();
	m_models.clear();
	m_costs.clear();
	m_models.resize(problem.numModels, 0);
	m_costs.resize(problem.numPieces);
	m_totalCost = 0;
	for ( size_t i = 0; i < m_costs.size(); i++ ) {
		Price p = problem.piecePrices[i] * problem.ownedPieces[i];
		m_costs[i] = -p;
		m_totalCost -= p;
	}
}

void
Individual::addModel ( size_t model )
{
	auto& problem = m_parent->getProblemData();

	m_models[model]++;
	for ( size_t i = 0; i < problem.numPieces; i++ ) {
		auto p = problem.models[model].prices[i];
		m_costs[i] += p;
		m_totalCost += p;
	}
}

void
Individual::removeModel ( size_t model )
{
	auto& problem = m_parent->getProblemData();

	m_models[model]--;
	for ( size_t i = 0; i < problem.numPieces; i++ ) {
		auto p = problem.models[model].prices[i];
		m_costs[i] -= p;
		m_totalCost -= p;
	}
}

void
Individual::switchModels ( size_t taken, size_t given )
{
	this->removeModel(taken);
	this->addModel(given);
}

void
Individual::doGreedyFill ( )
{
	while ( not this->areAllCostsOverValue(0) ) {
		size_t lowestTotalCostElem = std::min_element(m_costs.begin(), m_costs.end()) - m_costs.begin();
		size_t model = m_parent->m_bestModelsByPiece[lowestTotalCostElem];
		this->addModel(model);
	}
}

void
Individual::doRandomFill ( )
{
	while ( not this->areAllCostsOverValue(0) ) {
		size_t model = getRand() % m_models.size();
		this->addModel(model);
	}
}

#pragma endregion //}

#pragma region "Non-Modifying Methods" //{

void
Individual::print ( std::ostream& out )
const
{
	for ( auto&& g : m_models )
		out << g << " ";
	out << std::endl;
}

bool
Individual::isBetterThan ( const Individual& other )
const
{
	return m_totalCost < other.m_totalCost;
}


// private:

bool
Individual::areAllCostsOverValue ( Price threshold )
const
{
	return std::all_of(m_costs.begin(), m_costs.end(), [=] ( Price p ) { return p >= threshold; });
}

#pragma endregion //}

#pragma endregion //}

