import os.path
import sys
import subprocess


bestTime = None
bestThreshold = None
path = os.path.realpath("bin/quickSeuil")
for i in range(2, int(sys.argv[2]) + 1):
	proc_cmd = [path] + [sys.argv[1]] + ["-t"] + ["-r"] + [str(i)]
	proc_result = subprocess.run(proc_cmd, stdout=subprocess.PIPE)
	time = float(proc_result.stdout)
	print("%d : %f" % (i, time), end=", ", flush=True)
	if bestTime is None or time < bestTime:
		bestTime = time
		bestThreshold = i

print("\nBest threshold is " + str(bestThreshold))

