import math

with open("formulas.txt", "w+") as formulas_file:
	for i in range(2, 182, 10):
		formulas_file.write("=A%d\t=CONCATENATE(TEXT(B%d, \"0\"), \"-\", TEXT(B%d, \"0\"))\t=D%d\n" % (i, i, i + 9, i))