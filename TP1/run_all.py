import collections
import os.path
import sys
import subprocess


def find_all_files_in_dir(dir_path):
	all_files = []
	for root, subdirs, files in os.walk(dir_path):
		for f in files:
			filename = root + "/" + f
			filename = filename.replace("\\", "/")
			filename = filename.replace(dir_path.replace("\\", "/"), "")
			if filename[0] == "/":
				filename = filename[1:len(filename)]
			all_files.append(filename)
	
	return all_files


all_files = find_all_files_in_dir("exemplaires/")

# Add filenames in this dict to skip those files
excluded_files = {}

results = {}
for algo in ["counting", "quick", "quickSeuil", "quickRandomSeuil"]:
	results[algo] = {}
	for testset_filename in all_files:
		if testset_filename in excluded_files:
			continue
		path = os.path.realpath("bin/INF8775_TP1")
		split_filename = testset_filename.split("_")
		example_size = int(split_filename[1])
		example_no = int(split_filename[2].split(".")[0])
		proc_cmd = [path] + [algo] + ["exemplaires/" + testset_filename] + ["-t"]
		proc_result = subprocess.run(proc_cmd, stdout=subprocess.PIPE)
		print(algo + ": " + str(example_size) + "_" + str(example_no), end=", ", flush=True)
		if example_size not in results[algo]:
			results[algo][example_size] = {}
		try:
			results[algo][example_size][example_no] = float(proc_result.stdout)
		except ValueError:
			print("")
			print("Process returned '%s'" % str(proc_result.stdout), flush=True)
			exit()
	print("")

if not os.access("results", os.F_OK):
	os.mkdir("results");
for algo in results:
	with open("results/" + algo + ".csv", "w+") as results_file:
		results_file.write("Test set size\tTest set no\tTime to sort" + "\n")
		for size in sorted(results[algo]):
			for ex_no in sorted(results[algo][size]):
				results_file.write("%d\t%d\t%f\n" % (size, ex_no, results[algo][size][ex_no]))

