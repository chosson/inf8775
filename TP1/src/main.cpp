////////////////////////////////////////////////////////////////////////////////
/// \file main.cpp
/// \author Charles Hosson
/// \author Salha Yahia
///
/// 
////////////////////////////////////////////////////////////////////////////////


#pragma region "Includes" //{

#include <ciso646>
#include <cstddef>
#include <cstdint>
#include <new>

#include <algorithm>
#include <cmath>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <random>
#include <string>
#include <type_traits>
#include <unordered_map>
#include <utility>
#include <vector>

#if defined(_WIN32)
	#include <windows.h>
#elif defined(__unix__)
	#include <sys/time.h>
	#include <sys/resource.h>
#endif


using namespace std::literals;

#pragma endregion //}


#pragma region "Declarations" //{

#define RUN_AND_MEASURE_SORT(secondsVar, ...) \
{ \
	double t1 = getUserTime(); \
	__VA_ARGS__; \
	double t2 = getUserTime(); \
	secondsVar = double(t2 - t1); \
} \


using IntType = uint64_t;
using VecType = std::vector<IntType>;
using MapType = std::unordered_map<IntType, IntType>;


static constexpr int EXPERIMENTAL_RECURSION_THRESHOLD = 80; // Our experiments give an optimal threshold of average 80.
static constexpr int RADIX_SORT_BITS = 16;


struct ProgramOptions
{
	std::string algorithmName;
	std::string dataFilename;
	bool printTime = false;
	bool printResults = false;
	int recursionThreshold = EXPERIMENTAL_RECURSION_THRESHOLD;
};


double getUserTime ( );

ProgramOptions parseArgs ( int&, char** );

std::pair<VecType, bool> readFile ( const std::string& );

template < int >
void radixCountingSortImpl ( const VecType&, VecType&, int );

void countingSortImpl ( const VecType&, VecType&, IntType );

void countingSort ( const VecType&, VecType& );

void quickSort ( VecType& );

void quickSortExpThreshold ( VecType&, int );

void quickSortRandPivotExpThreshold ( VecType&, int );

void quickSortImpl ( VecType&, ptrdiff_t, ptrdiff_t, int = 1, bool = false );

ptrdiff_t partition ( VecType&, ptrdiff_t, ptrdiff_t, bool = false );

void insertionSort ( VecType::iterator, VecType::iterator );

#pragma endregion //}


int main ( int argc, char* argv[] )
{
	ProgramOptions opts = parseArgs(argc, argv);

	auto&& [data, ok] = readFile(opts.dataFilename);
	if ( not ok ) {
		std::cout << "Could not find file " << std::quoted(opts.dataFilename) << std::endl;
		return 1;
	}
	else if ( data.empty() ) {
		std::cout << "file " << std::quoted(opts.dataFilename) << " is empty" << std::endl;
		return 0;
	}

	VecType sorted = data;
	double seconds = 0.0;

	if ( opts.algorithmName == "counting" )
		RUN_AND_MEASURE_SORT(seconds, countingSort(data, sorted))
	else if ( opts.algorithmName == "quick" )
		RUN_AND_MEASURE_SORT(seconds, quickSort(sorted))
	else if ( opts.algorithmName == "quickSeuil" )
		RUN_AND_MEASURE_SORT(seconds, quickSortExpThreshold(sorted, opts.recursionThreshold))
	else if ( opts.algorithmName == "quickRandomSeuil" )
		RUN_AND_MEASURE_SORT(seconds, quickSortRandPivotExpThreshold(sorted, opts.recursionThreshold))
	else
		throw std::logic_error("Unrecognized algorithm '" + opts.algorithmName + "'");

	#ifdef TEST_RESULTS
		VecType expected = data;
		std::sort(expected.begin(), expected.end());
		std::cout << "Sorting Ok? " << std::boolalpha << (expected == sorted) << std::endl;
	#endif

	if ( opts.printTime )
		std::cout << (seconds * 1000.0) << std::endl;;

	if ( opts.printResults ) {
		for ( auto&& x : sorted )
			std::cout << x << " ";
		std::cout << std::endl;
	}
}


#pragma region "Functions" //{

double
getUserTime ( )
{
	double result = 0.0;

	#if defined(_WIN32)
		uint64_t userTimeRaw;
		FILETIME dummy;
		GetProcessTimes(GetCurrentProcess(), &dummy, &dummy, &dummy, (FILETIME*)&userTimeRaw);
		// User time is given in number of 100 nanoseconds intervals.
		result = userTimeRaw / 10'000'000.0;
	#elif defined(__unix__)
		rusage usage;
		getrusage(RUSAGE_SELF, &usage);
		timeval userTimeRaw = usage.ru_utime;
		uint64_t microseconds = userTimeRaw.tv_sec * 1'000'000 + userTimeRaw.tv_usec;
		result = microseconds / 1'000'000.0;
	#endif

	return result;
}


ProgramOptions
parseArgs ( int& argc, char** argv )
{
	ProgramOptions opts = {};

	if ( argc < 3 )
		throw std::logic_error("Must have algorithm and data set filename");
	opts.algorithmName = argv[1];
	opts.dataFilename = argv[2];

	for ( int i = 3; i < argc; i++ ) {
		if ( argv[i] == "-t"s )
			opts.printTime = true;
		else if ( argv[i] == "-p"s )
			opts.printResults = true;
		else if ( argv[i] == "-r"s )
			opts.recursionThreshold = std::stoi(argv[i + 1]);
	}

	return opts;
}


std::pair<VecType, bool>
readFile ( const std::string& filename )
{
	std::fstream file(filename, std::ios::in);
	if ( file.fail() )
		return {{}, false};

	VecType result;
	while ( not std::ws(file).eof() ) {
		result.push_back(0xFEDBABEBADC0FFEE);
		file >> result.back();
	}

	return {result, true};
}


template < int radixBits >
inline
void
radixCountingSortImpl ( const VecType& data, VecType& result, int exp )
{
	constexpr IntType MASK = (IntType(1) << radixBits) - 1;

	auto getIndex = [&] ( IntType elem )
	{
		return ((elem) >> (exp * radixBits)) & MASK;
	};

	VecType counts(MASK + 1);

	for ( IntType x : data )
		counts[getIndex(x)]++;

	for ( IntType i = 1; i <= MASK; i++ )
		counts[i] += counts[i - 1];

	for ( ptrdiff_t i = (ptrdiff_t)data.size() - 1; i >= 0; i-- ) {
		IntType index = getIndex(data[i]);
		result[counts[index] - 1] = data[i];
		counts[index]--;
	}
}


void
countingSortImpl ( const VecType& data, VecType& result, IntType max )
{
	MapType counts;
	counts.reserve(data.size());
	for ( IntType x : data )
		counts[x]++;

	for ( IntType i = 1; i <= max; i++ ) {
		if ( counts.count(i - 1) != 0 )
			counts[i] += counts[i - 1];
	}

	for ( ptrdiff_t i = (ptrdiff_t)data.size() - 1; i >= 0; i-- ) {
		result[counts[data[i]] - 1] = data[i];
		counts[data[i]]--;
	}
}


void
countingSort ( const VecType& data, VecType& result )
{

	#ifdef USE_RADIX_SORT_FOR_COUNTING_SORT
		VecType tmpData = data;
		//int numIter = (int)ceil(ceil(log2(maxElem + 1)) / RADIX_SORT_BITS);
		int numIter = (int)ceil(sizeof(IntType) * 8 / (double)RADIX_SORT_BITS);
		for ( int i = 0; i < numIter; i++ ) {
			radixCountingSortImpl<RADIX_SORT_BITS>(tmpData, result, i);
			tmpData = result;
		}
	#else
		auto maxElem = *std::max_element(data.begin(), data.end());
		countingSortImpl(data, result, maxElem);
	#endif
}


void
quickSort ( VecType& data )
{
	quickSortImpl(data, 0, data.size() - 1);
}


void
quickSortExpThreshold ( VecType& data, int recursionThreshold )
{
	quickSortImpl(data, 0, data.size() - 1, recursionThreshold);
}


void
quickSortRandPivotExpThreshold ( VecType& data, int recursionThreshold )
{
	quickSortImpl(data, 0, data.size() - 1, recursionThreshold, true);
}


void
quickSortImpl ( VecType& data, ptrdiff_t beg, ptrdiff_t end, int threshold, bool useRandomPivot )
{
	if ( beg < end ) {
		if ( end - beg < threshold ) {
			insertionSort(data.begin() + beg, data.begin() + end + 1);
			return;
		}

		ptrdiff_t pivot = partition(data, beg, end, useRandomPivot);
		quickSortImpl(data, beg, pivot - 1, threshold, useRandomPivot);
		quickSortImpl(data, pivot + 1, end, threshold, useRandomPivot);
	}
}


ptrdiff_t
partition ( VecType& data, ptrdiff_t beg, ptrdiff_t end, bool useRandomPivot )
{
	if ( useRandomPivot ) {
		static std::mt19937_64 gen(time(nullptr));
		std::uniform_int_distribution<ptrdiff_t> dist(beg, end);
		ptrdiff_t pivot = dist(gen);
		std::swap(data[end], data[pivot]);
	}

	// Lomuto partitioning
	ptrdiff_t index = beg;
	ptrdiff_t pivot = end;
	for ( ptrdiff_t i = beg; i < end; i++ ) {
		if ( data[i] < data[pivot] ) {
			std::swap(data[i], data[index]);
			index++;
		}
	}
	std::swap(data[pivot], data[index]);

	return index;
}


void
insertionSort ( VecType::iterator begin, VecType::iterator end )
{
	ptrdiff_t n = end - begin;
	for ( ptrdiff_t i = 1; i < n; i++) {
		IntType key = begin[i];
		ptrdiff_t j = i - 1;
		while ( j >= 0 and begin[j] > key ) {
			begin[j + 1] = begin[j];
			j = j - 1;
		}
		begin[j + 1] = key;
	}
}

#pragma endregion //}

